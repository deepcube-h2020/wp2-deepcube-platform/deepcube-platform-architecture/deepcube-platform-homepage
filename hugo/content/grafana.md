+++
author = ""
title = "Swarm Grafana"
image = "images/thumbs/grafana.jpg"
alt = "Swarm Grafana"
share = false
menu = ""
tags = ["ops"]
date = ""
+++

![grafana](/images/thumbs/grafana.jpg "Grafana") Grafana is the open source analytics & monitoring solution for every nodes and applications of the platform.

> Below you will find direct link.

https://grafana.prom.swarm.deepcube.gael-systems.com/